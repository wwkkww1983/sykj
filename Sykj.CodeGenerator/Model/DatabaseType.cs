﻿
namespace Sykj.CodeGenerator
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum DatabaseType
    {
        SqlServer,
        MySQL,
        PostgreSQL,
        Oracle
    }
}
