﻿
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;

namespace Sykj.Web.Controllers
{
    /// <summary>
    /// 百度编辑器后台地址
    /// </summary>
    public class UEditorController : Controller
    {
        private UEditorService ue;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ue"></param>
        public UEditorController(UEditorService ue)
        {
            this.ue = ue;
        }

        /// <summary>
        /// 处理请求
        /// </summary>
        public IActionResult Index()
        {
            ue.DoAction(HttpContext);
            return new EmptyResult();
        }
    }
}