﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.ViewModel;

namespace Sykj.Web.Api.v1
{
    /// <summary>
    /// 咨询管理
    /// </summary>
    public class GuestBookController : ApiController
    {
        #region 全局变量
        private readonly IGuestbook _guestbook;
        private readonly IUsers _users;
        private readonly IFavorite _favorite;
        #endregion

        #region 构造方法
        /// <summary>
        /// 构造方法
        /// </summary>
        public GuestBookController(IGuestbook guestbook, IUsers users, IFavorite favorite)
        {
            _guestbook = guestbook;
            _users = users;
            _favorite = favorite;
        }
        #endregion

        #region 咨询—留言列表 BJG
        /// <summary>
        ///咨询—留言列表 BJG
        /// </summary>
        /// <param name="pageIndex">起始页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="type">type:1:最新 ；2:热门</param>
        /// <param name="keyWords">搜索咨询内容</param>
        /// <returns></returns>
        [HttpGet("guestbooklist")]
        public ApiResult GuestbookList(int pageIndex, int pageSize, int type, string keyWords)
        {
            int userId = HeaderUserId;
            StringBuilder strWhere = new StringBuilder(" 1=1 AND Status=1 AND AuditStatus=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND Description.Contains(@0)");
            }
            string ordering = "Id desc";
            if (type == 1)
            {
                ordering = "CreatedDate Desc";
            }
            if (type == 2)
            {
                ordering = "BrowseCount Desc";
            }
            var list = _guestbook.GetPagedList(pageIndex, pageSize, strWhere.ToString(), ordering, keyWords).ToList();
            list.ForEach(c =>
            {
                var model = _users.GetModel(x => x.UserId == c.UserId);
                if (model != null)
                {
                    c.Gravatar = (!string.IsNullOrWhiteSpace(model.Gravatar)) ? (model.Gravatar.StartsWith("http") ? model.Gravatar : BaseConfig.ImagesDoMain + model.Gravatar) : BaseConfig.ImagesDoMain + "/upload/user/gravatar/default.jpg";
                    c.NickName = Utils.ReplaceChar(c.NickName);
                }
                else
                {
                    c.Gravatar = BaseConfig.ImagesDoMain + "/upload/user/gravatar/default.jpg";
                    c.NickName = Utils.ReplaceChar(c.NickName);
                }
                //查看用户是否关注
                var favModel = _favorite.GetModel(x => x.TargetId == c.Id && x.UserId == userId && x.Type == (int)EnumHelper.Favorite.Guestbook);
                if (favModel != null)
                    c.IsFavorite = true;
                else
                    c.IsFavorite = false;
            });
            return DataResult(list);
        }
        #endregion

        #region 咨询—提交留言 bjg
        /// <summary>
        /// 咨询—提交留言 bjg
        /// </summary>
        /// <returns></returns>
        [HttpPost("addguestbook")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public ApiResult AddGuestBook(string description)
        {
            Entity.Guestbook model = new Entity.Guestbook();
            model.BrowseCount = 0;
            model.CreatedDate = DateTime.Now;
            model.Description = description;
            model.UserId = UserId;
            var userModel = _users.GetModel(x => x.UserId == UserId);
            model.NickName = userModel.NickName ?? "";
            model.ParentId = 0;
            model.Privacy = 1;
            model.Status = 0;
            model.Title = "";
            bool b = _guestbook.Add(model);
            return AjaxResult(b);
        }
        #endregion

        #region 咨询—留言详情 bjg
        /// <summary>
        /// 咨询—留言详情 bjg
        /// </summary>
        /// <returns></returns>
        [HttpGet("guestbookdetail")]
        public ApiResult GuestBookDetail(int id)
        {
            int userId = HeaderUserId;
            var model = _guestbook.GetModel(x => x.Id == id);
            if (model == null)
            {
                return Failed("访问的问题不存在");
            }

            //获取数据成功 访问次数+1
            model.BrowseCount = model.BrowseCount + 1;
            bool b = _guestbook.Update(model);

            //留言人头像
            var usermodel = _users.GetModel(x => x.UserId == model.UserId);
            if (usermodel != null)
            {
                model.Gravatar = (usermodel.Gravatar != null) ? (usermodel.Gravatar.StartsWith("http") ? usermodel.Gravatar : BaseConfig.ImagesDoMain + usermodel.Gravatar) : BaseConfig.ImagesDoMain + "/upload/user/gravatar/default.jpg";
                model.NickName = Utils.ReplaceChar(model.NickName); ;
            }
            else
            {
                model.Gravatar = BaseConfig.ImagesDoMain + "/upload/user/gravatar/default.jpg";
                model.NickName = Utils.ReplaceChar(model.NickName);
            }

            //处理人头像
            model.HandlerGravatar = "";
            if (model.HandlerUserId != null)
            {
                var handlerModel = _users.GetModel(x => x.UserId == model.HandlerUserId);
                if (handlerModel != null)
                {
                    model.HandlerGravatar = (!string.IsNullOrWhiteSpace(handlerModel.Gravatar)) ? (handlerModel.Gravatar.StartsWith("http") ? handlerModel.Gravatar : BaseConfig.ImagesDoMain + handlerModel.Gravatar) : BaseConfig.ImagesDoMain + "/upload/user/gravatar/default.jpg";
                }
                else
                {
                    model.HandlerGravatar = BaseConfig.ImagesDoMain + "/upload/user/gravatar/default.jpg";
                }
            }
            //查看用户是否关注
            var favModel = _favorite.GetModel(x => x.TargetId == id && x.UserId == userId && x.Type == (int)EnumHelper.Favorite.Guestbook);
            if (favModel != null)
                model.IsFavorite = true;
            else
                model.IsFavorite = false;
            return DataResult(model);
        }
        #endregion
    }
}