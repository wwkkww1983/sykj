﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sykj.Components;
using Sykj.Infrastructure;
using Sykj.IServices;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ConfigController : MController
    {
        IConfigsys _configsys;
        ICacheService _cacheService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="configsys"></param>
        /// <param name="cacheService"></param>
        public ConfigController(IConfigsys configsys, ICacheService cacheService)
        {
            _configsys = configsys;
            _cacheService = cacheService;
        }

        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        /// <returns></returns>
        public IActionResult List(int page, int limit, string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND KeyName.Contains(@0) ");
            }

            var list = _configsys.GetPagedList(page, limit, strWhere.ToString(), " csId desc ", keyWords);
            int recordCount = _configsys.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            ViewBag.Types = new List<SelectListItem>
            {
                new SelectListItem { Value = "1", Text = "系统" },
                new SelectListItem { Value = "2", Text = "其他" }
            };
            return View();
        }

        /// <summary>
        /// 添加保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]
        public IActionResult Add(Entity.Configsys configsys)
        {
            configsys.CreateDate = DateTime.Now;
            bool b = _configsys.Add(configsys);
            _cacheService.RemoveCache(CacheKey.CONFIGSYSTEMLISTALL);
            Components.LogOperate.Add("添加系统参数", "添加ID为【" + configsys.CsId + "】的参数", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public IActionResult Edit(int id)
        {
            var model = _configsys.GetModel(c => c.CsId == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            ViewBag.Types = new List<SelectListItem>
            {
                new SelectListItem { Value = "1", Text = "系统" },
                new SelectListItem { Value = "2", Text = "其他" }
            };
            return View(model);
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]
        public IActionResult Edit(Entity.Configsys configsys)
        {
            var model = _configsys.GetModel(c => c.CsId == configsys.CsId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.KeyValue = configsys.KeyValue;
            model.Description = configsys.Description;
            bool b = _configsys.Update(model);
            _cacheService.RemoveCache(CacheKey.CONFIGSYSTEMLISTALL);
            Components.LogOperate.Add("编辑系统参数", "编辑ID为【" + configsys.CsId + "】的参数", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteAll(List<int> ids)
        {
            var list = _configsys.GetList(c => ids.Contains(c.CsId));
            bool b = _configsys.Delete(list);
            Components.LogOperate.Add("批量删除系统参数", "删除ID为【" + string.Join(",", ids) + "】的参数", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 网站设置
        /// </summary>
        /// <returns></returns>
        public IActionResult WebSite()
        {
            return View();
        }

        /// <summary>
        /// 参数配置
        /// </summary>
        /// <returns></returns>
        public IActionResult ParamConfig()
        {
            return View();
        }

        /// <summary>
        /// 参数配置
        /// </summary>
        /// <param name="wxJsApiKey"></param>
        /// <param name="wxNativeApiKey"></param>
        /// <param name="wxApiKey"></param>
        /// <param name="wechatAppID"></param>
        /// <param name="wechatAppSecret"></param>
        /// <param name="wxAppID"></param>
        /// <param name="wxAppSecret"></param>
        /// <param name="wechatFWAppID"></param>
        /// <param name="wechatFWAppSecret"></param>
        /// <param name="wxPcAppID"></param>
        /// <param name="wxPcAppSecret"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ParamConfig(string wxJsApiKey, string wxNativeApiKey, string wxApiKey, string wechatAppID, string wechatAppSecret, string wxAppID, string wxAppSecret, string wechatFWAppID, string wechatFWAppSecret, string wxPcAppID, string wxPcAppSecret)
        {
            //微信支付
            var model = _configsys.GetModel(x => x.KeyName == Constant.WXJSAPIKEY);
            model.KeyValue = wxJsApiKey;
            bool b1 = _configsys.Update(model, true, c => c.KeyValue);

            var model2 = _configsys.GetModel(x => x.KeyName == Constant.WXNATIVEAPIKEY);
            model2.KeyValue = wxNativeApiKey;
            bool b2 = _configsys.Update(model2, true, c => c.KeyValue);

            var model3 = _configsys.GetModel(x => x.KeyName == Constant.WXAPIKEY);
            model3.KeyValue = wxApiKey;
            bool b3 = _configsys.Update(model3, true, c => c.KeyValue);

            var model4 = _configsys.GetModel(x => x.KeyName == Constant.WECHATAPPID);
            model4.KeyValue = wechatAppID;
            bool b4 = _configsys.Update(model4, true, c => c.KeyValue);

            var model5 = _configsys.GetModel(x => x.KeyName == Constant.WECHATAPPSECRET);
            model5.KeyValue = wechatAppSecret;
            bool b5 = _configsys.Update(model5, true, c => c.KeyValue);

            var model6 = _configsys.GetModel(x => x.KeyName == Constant.WXAPPID);
            model6.KeyValue = wxAppID;
            bool b6 = _configsys.Update(model6, true, c => c.KeyValue);

            var model7 = _configsys.GetModel(x => x.KeyName == Constant.WXAPPSECRET);
            model7.KeyValue = wxAppSecret;
            bool b7 = _configsys.Update(model7, true, c => c.KeyValue);
    
            var model8= _configsys.GetModel(x => x.KeyName == Constant.WECHATFWAPPID);
            model8.KeyValue = wechatFWAppID;
            bool b8 = _configsys.Update(model8, true, c => c.KeyValue);

            var model9 = _configsys.GetModel(x => x.KeyName == Constant.WECHATFWAPPSECRET);
            model9.KeyValue = wechatFWAppSecret;
            bool b9 = _configsys.Update(model9, true, c => c.KeyValue);
     
            var model10 = _configsys.GetModel(x => x.KeyName == Constant.WXPCAPPID);
            model10.KeyValue = wxPcAppID;
            bool b10 = _configsys.Update(model10, true, c => c.KeyValue);

            var model11 = _configsys.GetModel(x => x.KeyName == Constant.WXPCAPPSECRET);
            model11.KeyValue = wxPcAppSecret;
            bool b11 = _configsys.Update(model11, true, c => c.KeyValue);

            bool b = b1 && b2 && b3 && b4 && b5 && b6 && b7 && b8 && b9 && b10 && b11;
            _cacheService.RemoveCache(CacheKey.CONFIGSYSTEMLISTALL);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 保存参数配置
        /// </summary>
        /// <param name="webSite">网站域名</param>
        /// <param name="baseHost">主域名</param>
        /// <param name="apiHost">接口域名</param>
        /// <param name="imagesDomain">图片域名</param>
        /// <param name="registerProtocol">注册协议</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult WebSite(string webSite, string baseHost, string apiHost, string imagesDomain, string registerProtocol)
        {
            //网站
            var model = _configsys.GetModel(x => x.KeyName == Constant.WEBSITE);
            model.KeyValue = webSite;
            bool b1 = _configsys.Update(model, true, c => c.KeyValue);
            //主域名
            var model2 = _configsys.GetModel(x => x.KeyName == Constant.DOMAIN);
            model2.KeyValue = baseHost;
            bool b2 = _configsys.Update(model2, true, c => c.KeyValue);
            //api域名
            var model3 = _configsys.GetModel(x => x.KeyName == Constant.APIDOMAIN);
            model3.KeyValue = apiHost;
            bool b3 = _configsys.Update(model3, true, c => c.KeyValue);
            //图片地址
            var model4 = _configsys.GetModel(x => x.KeyName == Constant.IMAGESDOMAIN);
            model4.KeyValue = imagesDomain;
            bool b4 = _configsys.Update(model4, true, c => c.KeyValue);
            //注册协议
            var model5 = _configsys.GetModel(x => x.KeyName == Constant.REGISTERPORTOCOL);
            model5.KeyValue = registerProtocol;
            bool b5 = _configsys.Update(model5, true, c => c.KeyValue);

            bool b = b1 && b2 && b3 && b4 && b5;
            _cacheService.RemoveCache(CacheKey.CONFIGSYSTEMLISTALL);
            return Json(AjaxResult(b));
        }
    }
}