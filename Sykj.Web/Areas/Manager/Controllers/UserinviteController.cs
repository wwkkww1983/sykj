/*
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：推广表                                                    
*│　作    者：bjg                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-04-03 10:51:43       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Web.Areas.Manager.Controllers                         
*│　类    名： UserinviteController                                 
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.IServices;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 推广表
    /// </summary>
    public class UserinviteController : MController
    {
        IUserinvite _userinvite;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="userinvite"></param>
        public UserinviteController(IUserinvite userinvite)
        {
            _userinvite = userinvite;
        }

        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键字</param>
        /// <returns></returns>
        public IActionResult List(int page, int limit,string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (UserNick.Contains(@0) OR InviteNick.Contains(@0) )");
            }
            var list = _userinvite.GetPagedListExt(page, limit, strWhere.ToString(), " Id desc ", keyWords);
            int recordCount = _userinvite.GetCountExt(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 添加保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
		[ModelValidation]//服务器表单验证
        public IActionResult Add(Sykj.Entity.Userinvite model)
        {
            bool b = _userinvite.Add(model);
            Components.LogOperate.Add("添加推广信息", "添加ID为【" + model.Id + "】的推广信息", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public IActionResult Edit(int id)
        {
            var model = _userinvite.GetModel(c => c.Id == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
		[ModelValidation]//服务器表单验证
        public IActionResult Edit(Sykj.Entity.Userinvite userinvite)
        {
            var model = _userinvite.GetModel(c => c.Id == userinvite.Id);
            if (model == null)
            { 
                return Json(Failed( "查询不到该数据"));
            }
            bool b = _userinvite.Update(model);
            Components.LogOperate.Add("编辑推广信息", "编辑ID为【" + model.Id + "】的推广信息", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteAll(List<int> ids)
        {
            var list = _userinvite.GetList(c => ids.Contains(c.Id));
            bool b = _userinvite.Delete(list);
            Components.LogOperate.Add("批量删除推广信息", "删除ID为【" + string.Join(",", ids) + "】的推广信息", HttpContext);
            return Json(AjaxResult(b));
        }
    }
}