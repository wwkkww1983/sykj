﻿/*******************************************************************************
* Copyright (C) sykjwh.cn
* 
* Author: liuxiang
* Create Date: 2018/03/08 
* Description: Automated building by liuxiang20041986@qq.com 
* http://www.sykjwh.cn/
*********************************************************************************/

using Microsoft.EntityFrameworkCore;

namespace Sykj.Repository
{
    /// <summary>
    /// 数据库上下文类
    /// </summary>
    public partial class SyDbContext : DbContext
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="options"></param>
        public SyDbContext(DbContextOptions<SyDbContext> options) : base(options)
        {

        }

        public virtual DbSet<Sykj.Entity.Permissions> Permissions { get; set; }
        public virtual DbSet<Sykj.Entity.Rolepermissions> Rolepermissions { get; set; }
        public virtual DbSet<Sykj.Entity.Roles> Roles { get; set; }
        public virtual DbSet<Sykj.Entity.Users> Users { get; set; }
        public virtual DbSet<Sykj.Entity.Usertype> Usertype { get; set; }
        public virtual DbSet<Sykj.Entity.Log> Log { get; set; }
        public virtual DbSet<Sykj.Entity.Configsys> Configsys { get; set; }
        public virtual DbSet<Sykj.Entity.UserRoles> UserRoles { get; set; }
        public virtual DbSet<Sykj.Entity.Advertisement> Advertisement { get; set; }
        public virtual DbSet<Sykj.Entity.Advertiseposition> Advertiseposition { get; set; }
        public virtual DbSet<Sykj.Entity.Channel> Channel { get; set; }
        public virtual DbSet<Sykj.Entity.Content> Content { get; set; }
        public virtual DbSet<Sykj.Entity.Module> Module { get; set; }
        public virtual DbSet<Sykj.Entity.Images> Images { get; set; }
        public virtual DbSet<Sykj.Entity.Regions> Regions { get; set; }
        public virtual DbSet<Sykj.Entity.Usersexp> Usersexp { get; set; }
        public virtual DbSet<Sykj.Entity.ThumbnailSize> ThumbnailSize { get; set; }
        public virtual DbSet<Sykj.Entity.Guestbook> Guestbook { get; set; }
        public virtual DbSet<Sykj.Entity.Favorite> Favorites { get; set; }
        public virtual DbSet<Sykj.Entity.PointsDetail> PointsDetail { get; set; }
        public virtual DbSet<Sykj.Entity.SiteMessage> SiteMessage { get; set; }
        public virtual DbSet<Sykj.Entity.HotKeyWords> HotKeyWords { get; set; }
        public virtual DbSet<Sykj.Entity.Userinvite> Userinvite { get; set; }
        public virtual DbSet<Sykj.Entity.Dic> Dic { get; set; }

        /// <summary>
        /// 映射配置调用
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //应用映射配置 
            modelBuilder.ApplyConfiguration(new LogMap());
            modelBuilder.ApplyConfiguration(new UsersMap());
            modelBuilder.ApplyConfiguration(new RolesMap());
            modelBuilder.ApplyConfiguration(new PermissionsMap());
            modelBuilder.ApplyConfiguration(new RolepermissionsMap());
            modelBuilder.ApplyConfiguration(new UserRolesMap());
            modelBuilder.ApplyConfiguration(new UsertypeMap());
            modelBuilder.ApplyConfiguration(new ConfigsysMap());
            modelBuilder.ApplyConfiguration(new AdvertisementMap());
            modelBuilder.ApplyConfiguration(new AdvertisepositionMap());
            modelBuilder.ApplyConfiguration(new ChannelMap());
            modelBuilder.ApplyConfiguration(new ContentMap());
            modelBuilder.ApplyConfiguration(new ModuleMap());
            modelBuilder.ApplyConfiguration(new ImagesMap());
            modelBuilder.ApplyConfiguration(new RegionsMap());
            modelBuilder.ApplyConfiguration(new UsersexpMap());
            modelBuilder.ApplyConfiguration(new ThumbnailSizeMap());
            modelBuilder.ApplyConfiguration(new GuestbookMap());
            modelBuilder.ApplyConfiguration(new FavoriteMap());
            modelBuilder.ApplyConfiguration(new PointsDetailMap());
            modelBuilder.ApplyConfiguration(new SiteMessageMap());
            modelBuilder.ApplyConfiguration(new HotkeywordsMap());
            modelBuilder.ApplyConfiguration(new DicMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}
