﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class RolepermissionsMap : IEntityTypeConfiguration<Sykj.Entity.Rolepermissions>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Rolepermissions> entity)
        {
            entity.HasKey(e => new { e.Id });

            entity.ToTable("accounts_rolepermissions");

        }
    }
}
