﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using System.Collections.Generic;

namespace Sykj.Test.Manager
{
    public class SiteMessage: IDisposable
    {
        IServiceProvider _serviceProvider;
        ISiteMessage _siteMessage;
        IUsers _users;
        SiteMessageController _siteMessageController;

        public SiteMessage()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _siteMessage = _serviceProvider.GetService<ISiteMessage>();
            _users = _serviceProvider.GetService<IUsers>();
            _siteMessageController = new SiteMessageController(_siteMessage, _users);
        }

        /// <summary>
        /// 多场景参数测试
        /// </summary>
        [Theory]
        [InlineData(1, 10)]
        [InlineData(2, 10)]
        [InlineData(3, 10)]
        public void List(int pageIndex,int pageSize)
        {
            JsonResult r = _siteMessageController.List(pageIndex, pageSize, "") as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 添加保存
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void Add()
        {
            _siteMessageController.UserId = 88;
            Sykj.Entity.SiteMessage model = new Entity.SiteMessage()
            {
                MsgType= "System",
                Title="单元测试",
                Content="单元测试内容",
            };
            model.ReceiverUserNameList.Add("18206715217");
            JsonResult r = _siteMessageController.Add(model) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void AddEditDelete()
        {
            //增
            _siteMessageController.UserId = 3;
            Sykj.Entity.SiteMessage model = new Entity.SiteMessage()
            {
                MsgType = "System",
                Title = "单元测试",
                Content = "单元测试内容",
            };
            model.ReceiverUserNameList.Add("18206715217");
            JsonResult r = _siteMessageController.Add(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            ////删
            //List<int> list = new List<int>();
            //list.Add(model.Id);
            //r = _siteMessageController.DeleteAll(list) as JsonResult;
            //ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success);
        }

        /// <summary>
        /// 清除测试数据
        /// </summary>
        public void Dispose()
        {
            //清除测试数据
        }
    }
}
