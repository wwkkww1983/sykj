﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;
using Sykj.Infrastructure;

namespace Sykj.Test.Manager
{
    /// <summary>
    /// 高校信息
    /// </summary>
    public class Users : IDisposable
    {
        IServiceProvider _serviceProvider;
        IUsers _users;
        IRoles _roles;
        UsersController _usersController;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Users()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _users = _serviceProvider.GetService<IUsers>();
            _roles = _serviceProvider.GetService<IRoles>();
            _usersController = new UsersController(_users, _roles);
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        [Theory]
        [InlineData(1,10,"")]
        public void List(int page, int limit, string keyWords)
        {
            JsonResult r = _usersController.List(page, limit, keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void AddEditDelete()
        {
            //增
            Sykj.Entity.Users model = new Entity.Users();
            model.UserName= "1777777777";
            model.Password = "123456";
            model.TrueName = "班金国";
            model.Phone = "1777777777";
            model.Email = "1777777777";
            model.Activity = true;
            model.RoleIds = new List<int>();
            model.RoleIds.Add(1);
            model.RoleIds.Add(2);
            JsonResult r = _usersController.Add(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //改
            model.TrueName = "班金国";
            model.Phone = "1777777777";
            model.Email = "1777777777";
            model.Activity = true;
            model.RoleIds = new List<int>();
            model.RoleIds.Add(1);
            model.RoleIds.Add(2);
            r = _usersController.Edit(model) as JsonResult;
            ApiResult result2 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //删
            List<int> list = new List<int>();
            list.Add(model.UserId);
            r = _usersController.DeleteAll(list) as JsonResult;
            ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success&& result3.success);
        }

        public void Dispose()
        {
        }
    }
}
