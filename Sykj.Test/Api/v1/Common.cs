﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using System.Linq;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.JsonWebTokens;
using Sykj.ViewModel;
using Sykj.Components;

namespace Sykj.Test.Api.v1
{
    public class Common : ApiController
    {
        IServiceProvider _serviceProvider;
        IRegions _regions;

        CommonController _commonController;

        public Common()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _regions = _serviceProvider.GetService<IRegions>();
            _commonController = new CommonController(_regions);
        }

        /// <summary>
        /// 测试省份接口
        /// </summary>
        /// <param name="parentId"></param>
        [Theory]
        [InlineData(0)]
        public void NewRegionList(int parentId)
        {
            var result = _commonController.NewRegionList(parentId);
            Assert.True(result.success);
        }
        /// <summary>
        /// 测试配置参数
        /// </summary>
        /// <param name="keyValue"></param>
        [Theory]
        [InlineData("ContactUs")]
        public void GetSystemParameter(string keyValue)
        {
            var result = _commonController.GetSystemParameter(keyValue);
            Assert.True(result.success);
        }

    }
}
