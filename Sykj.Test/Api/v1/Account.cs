﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Components;

namespace Sykj.Test.Api.v1
{
    public class Account : IDisposable
    {
        IServiceProvider _serviceProvider;
        IUsers _users;
        IUserMember _userMember;
        ICacheService _cacheService;
        AccountController _accountController;

        public Account()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _users = _serviceProvider.GetService<IUsers>();
            _userMember = _serviceProvider.GetService<IUserMember>();
            _cacheService = _serviceProvider.GetService<ICacheService>();
            _accountController = new AccountController(_users, _userMember, _cacheService);//实例化
        }

        /// <summary>
        /// 测试登录 bjg
        /// </summary>
        [Fact]
        public void Login()
        {
            ApiResult result = _accountController.Login("18213971239", "123456");
            Assert.True(result.success);
        }

        /// <summary>
        /// 注册协议 bjg
        /// </summary>
        [Fact]
        public void RegisterProtocol()
        {
            ApiResult result = _accountController.RegisterProtocol();
            Assert.True(result.success);
        }

        /// <summary>
        ///发送注册短信(如果用户名已注册 false为正确 用户名不存在时为true) bjg
        /// </summary>
        [Fact]
        public void SendSms()
        {
            var data = _accountController.ValidateCode();//获取图片验证码
            ApiResult result = _accountController.SendSms("18213971239 ", "", "");
            Assert.True(result.success);
        }

        /// <summary>
        /// 注册 bjg
        /// </summary>
        [Fact]
        public void Register()
        {
            var ValidateCode = _accountController.ValidateCode();//获取图片验证码编号
            var smsData = _accountController.SendSms("18213971239 ", "", "");//获得短信验证码
            ApiResult result = _accountController.Register("18213971239 ", "123456", "");//注册
            Assert.True(result.success);
        }

        /// <summary>
        /// 找回密码（修改） bjg
        /// </summary>
        [Fact]
        public void FindPassword()
        {
            var ValidateCode = _accountController.ValidateCode();//获取图片验证码编号
            var smsData = _accountController.SendFindpwd("18213971239", "", "");//获得短信验证码
            ApiResult result = _accountController.FindPassword("18213971239 ", "", "", "");//注册
            Assert.True(result.success);
        }

        /// <summary>
        /// 测试发送短信验证码
        /// </summary>
        [Fact]
        public void SendLoginCode()
        {
            //短信验证码登录
            ApiResult result = _accountController.SendLoginCode("18213971239 ", "12345", "");//注册
            Assert.True(result.success);
        }


        /// <summary>
        /// 验证码登录 bjg
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="code">短信验证码</param>
        /// <returns></returns>
        [Fact]
        public void LoginCode()
        {
            //短信验证码登录
            ApiResult result = _accountController.LoginCode("18213971239 ", "12345");//注册
            Assert.True(result.success);
        }

        /// <summary>
        /// 清除测试数据
        /// </summary>
        public void Dispose()
        {
            //清除测试数据
        }
    }
}
