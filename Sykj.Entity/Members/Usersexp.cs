/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：用户扩展表                                                    
*│　作    者：liuxiang                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-01-21 16:44:28 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Entity                                  
*│　类    名：Usersexp                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
	/// <summary>
	/// 用户扩展表/会员信息
	/// </summary>
	public class Usersexp
	{
		/// <summary>
		/// 主键
		/// </summary>
		public Int32 UserId {get;set;}

		/// <summary>
		/// 个性签名
		/// </summary>
		[MaxLength(200)]
		public String Singature {get;set;}

		/// <summary>
		/// 电话号码
		/// </summary>
		[MaxLength(50)]
		public String TelPhone {get;set;}

		/// <summary>
		/// QQ号
		/// </summary>
		[MaxLength(50)]
		public String QQ {get;set;}

		/// <summary>
		/// 主页
		/// </summary>
		[MaxLength(50)]
		public String HomePage {get;set;}

		/// <summary>
		/// 生日
		/// </summary>
		public DateTime Birthday {get;set;}

		/// <summary>
		/// 籍贯
		/// </summary>
		[MaxLength(50)]
		public String NativePlace {get;set;}

		/// <summary>
		/// 地区Id
		/// </summary>
		[MaxLength(100)]
		public String RegionId {get;set;}

		/// <summary>
		/// 住址
		/// </summary>
		[MaxLength(200)]
		public String Address {get;set;}

		/// <summary>
		/// 等级
		/// </summary>
		public Int32 Grade {get;set;}

		/// <summary>
		///  账户余额
		/// </summary>
		public Decimal? Balance {get;set;}

		/// <summary>
		/// 积分
		/// </summary>
		public Int32? Points {get;set;}

		/// <summary>
		/// 用户是否通过实名认证
		/// </summary>
		public bool? IsUserDPI {get;set;}

		/// <summary>
		/// 会员卡
		/// </summary>
		[MaxLength(50)]
		public String UserCardCode {get;set;}

		/// <summary>
		/// 会员卡类型
		/// </summary>
		public Int32? UserCardType {get;set;}

		/// <summary>
		///  1:APP注册 2:微信公众号注册 3:PC 4:微信小程序 
		/// </summary>
		public Int32? SourceType {get;set;}

		/// <summary>
		/// 身份证号
		/// </summary>
		[MaxLength(100)]
		public String CardId {get;set;}

		/// <summary>
		/// 修改时间
		/// </summary>
		public DateTime? UpdateDate {get;set;}

		/// <summary>
		/// 备注
		/// </summary>
		public String Remark {get;set;}

		/// <summary>
		/// 最后登录时间
		/// </summary>
		public DateTime? LastLoginDate {get;set;}


        /// <summary>
        /// 会员到期时间
        /// </summary>
        public DateTime? ExpireTime { get; set; }
    }
}
