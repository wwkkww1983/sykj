/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：积分明细                                                    
*│　作    者：ydp                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-06 14:26:00 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Entity                                  
*│　类    名：Pointsdetail                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
	/// <summary>
	/// 积分明细
	/// </summary>
	public class PointsDetail
	{
		/// <summary>
		/// 流水帐号 ID
		/// </summary>
		public Int32 PointId {get;set;}

		/// <summary>
		/// 积分操作ID   特殊值处理 -1：表示购买商品所得积分
		/// </summary>
		[Required(ErrorMessage = "请输入积分操作ID   特殊值处理 -1：表示购买商品所得积分")]
		public Int32 RuleId {get;set;}

		/// <summary>
		///  用户Id
		/// </summary>
		[Required(ErrorMessage = "请输入 用户Id")]
		public Int32 UserId {get;set;}

		/// <summary>
		/// 影响分数
		/// </summary>
		[Required(ErrorMessage = "请输入影响分数")]
		public Int32 Score {get;set;}

		/// <summary>
		/// 当前积分
		/// </summary>
		[Required(ErrorMessage = "请输入当前积分")]
		public Int32 CurrentPoints {get;set;}

		/// <summary>
		/// 积分详细
		/// </summary>
		[MaxLength(255)]
		public String Description {get;set;}

		/// <summary>
		///  0:表示积分的获得，1：表示积分的消费
		/// </summary>
		[Required(ErrorMessage = "请输入 0:表示积分的获得，1：表示积分的消费")]
		public Int32 Type {get;set;}

		/// <summary>
		/// 创建时间
		/// </summary>
		[Required(ErrorMessage = "请输入创建时间")]
		public DateTime CreatedDate {get;set;}


	}
}
