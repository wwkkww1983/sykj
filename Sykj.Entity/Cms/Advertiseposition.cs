﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 广告位
    /// </summary>
    public partial class Advertiseposition
    {
        /// <summary>
        /// 广告位编号
        /// </summary>
        [Key]
        public int AdvPositionId { get; set; }
        /// <summary>
        /// 广告位名称
        /// </summary>
        [Display(Name = "广告位名称")]
        [Required(ErrorMessage = "此项不能为空")]
        public string AdvPositionName { get; set; }
        /// <summary>
        /// 显示类型    0：纵向平铺  1：横向平铺  2：层叠显示 3：交替显示  4：自定义广告代码
        /// </summary>
        [Display(Name = "显示类型")]
        [Required(ErrorMessage = "此项不能为空")]
        public int ShowType { get; set; }
        /// <summary>
        /// 横向平铺时 行显示个数
        /// </summary>
        [Display(Name = "行显示个数")]
        [Required(ErrorMessage = "此项不能为空")]
        public int? RepeatColumns { get; set; }
        /// <summary>
        /// 宽度
        /// </summary>
        [Display(Name = "宽度")]
        public int? Width { get; set; }
        /// <summary>
        /// 高度
        /// </summary>
        [Display(Name = "高度")]
        public int? Height { get; set; }
        /// <summary>
        /// 自定义代码
        /// </summary>
        [Display(Name = "广告位内容")]
        public string AdvHtml { get; set; }
        /// <summary>
        /// 是否单一广告，还是循环广告
        /// </summary>
        [Display(Name = "循环显示")]
        public bool IsOne { get; set; }
        /// <summary>
        /// 循环广告 循环间隔
        /// </summary>
        [Display(Name = "循环间隔")]
        public int? TimeInterval { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public int CreateUserId { get; set; }
    }
}
