﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 图片管理
    /// </summary>
    public partial class Images
    {
        /// <summary>
        /// 表Id
        /// </summary>
        public int ImageId { get; set; }
        /// <summary>
        /// 所属Id
        /// </summary>
        public int TargetId { get; set; }
        /// <summary>
        /// 原始地址
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 缩略图地址
        /// </summary>
        public string ThumbnailUrl { get; set; }
        /// <summary>
        /// 原文件名
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 文件扩展名
        /// </summary>
        public string FileExt { get; set; }
        /// <summary>
        /// 文件大小
        /// </summary>
        public string FileSize { get; set; }
        /// <summary>
        /// 所属表名
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; } = DateTime.Now;
    }
}
