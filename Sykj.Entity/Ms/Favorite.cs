/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：bjg                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-02-21 09:38:35 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Entity                                  
*│　类    名：Favorite                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 收藏信息表
    /// </summary>
    public class Favorite
    {
        /// <summary>
        /// 收藏Id
        /// </summary>
        public Int32 FavoriteId { get; set; }

        /// <summary>
        /// 收藏类型:   4:高校 5:专业  6：问题
        /// </summary>
        [Required(ErrorMessage = "请输入收藏类型:   4:高校 5:专业  6：问题")]
        public Int32 Type { get; set; }

        /// <summary>
        /// 收藏目标Id
        /// </summary>
        [Required(ErrorMessage = "请输入收藏目标Id")]
        public Int32 TargetId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        [Required(ErrorMessage = "请输入用户Id")]
        public Int32 UserId { get; set; }

        /// <summary>
        ///  收藏标签
        /// </summary>
        [MaxLength(255)]
        public String Tags { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [MaxLength(255)]
        public String Remark { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        [Required(ErrorMessage = "请输入创建日期")]
        public DateTime CreatedDate { get; set; }

        #region 扩展属性 bjg
        /// <summary>
        /// 高校Id
        /// </summary>
        [NotMapped]
        public int? CollegeId { get; set; }
        /// <summary>
        /// 高校LOGO
        /// </summary>
        [NotMapped]
        public String Logo { get; set; }
        /// <summary>
        /// 高校名称
        /// </summary>
        [NotMapped]
        public String CollegeName { get; set; }
        /// <summary>
        ///教育属性
        /// </summary>
        [NotMapped]
        public String EduAttrName { get; set; }
        /// <summary>
        ///院校类型
        /// </summary>
        [NotMapped]
        public String SubScopeName { get; set; }
        /// <summary>
        ///学科层次
        /// </summary>
        [NotMapped]
        public String SchTypeName { get; set; }
        /// <summary>
        ///高校地区id
        /// </summary>
        [NotMapped]
        public String RegionId { get; set; }
        /// <summary>
        ///高校地区
        /// </summary>
        [NotMapped]
        public String RegionName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary>
        [NotMapped]
        public int? MajorId { get; set; }
        /// <summary>
        /// 专业编码
        /// </summary>
        [NotMapped]
        public string MajorCode { get; set; }
        /// <summary>
        /// 专业名称
        /// </summary>
        [NotMapped]
        public string MajorName { get; set; }

        /// <summary>
        /// 问题Id
        /// </summary>
        [NotMapped]
        public int? GuestbookId { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        [NotMapped]
        public string Gravatar { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        [NotMapped]
        public string NickName { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [NotMapped]
        public string Description { get; set; }
        #endregion
    }
}
