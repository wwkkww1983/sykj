﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 会员信息
    /// </summary>
    public interface IUserMember : Sykj.Repository.IRepository<Sykj.Entity.Usersexp>
    {
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="passWord">密码</param>
        /// <param name="nikeName">昵称</param>
        /// <param name="wechatOpenId">微信openId</param>
        /// <param name="unionId">腾讯平台用户唯一标识</param>
        /// <returns></returns>
        int Register(string userName, string passWord, string nikeName = "", string wechatOpenId = "", string unionId = "",string gavatarUrl="",int inviteUserId = 0);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        IQueryable<Sykj.ViewModel.UserMember> GetMemberList(int pageIndex,
           int pageSize, string predicate, string ordering, params object[] args);

        /// <summary>
        /// 用户信息
        /// </summary>
        /// <param name="predicate">条件</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        Sykj.ViewModel.UserMember GetMemberModel(Expression<Func<Sykj.ViewModel.UserMember, bool>> predicate);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        int GetMemberRecordCount(string predicate, params object[] args);
    }
}
