﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Sykj.IServices
{
    /// <summary>
    /// 省市区的接口
    /// </summary>
    public interface IRegions : Sykj.Repository.IRepository<Sykj.Entity.Regions>
    {
        /// <summary>
        /// 从缓存中获取数据所有集合
        /// </summary>
        /// <returns>返回满足查询条件的list</returns>
        List<Sykj.Entity.Regions> GetListByCache();

        /// <summary>
        /// 返回省市区模型
        /// </summary>
        /// <param name="predicate">参数</param>
        /// <returns></returns>
        Sykj.Entity.Regions GetModelByCache(Func<Entity.Regions, bool> predicate);

        /// <summary>
        /// 根据任何地区ID，获取地区完整名称集合
        /// </summary>    
        ///<param name="regionId"></param> 
        ///<return>返回名称</return>
        List<string> GetRegionNameByRID(string regionId);

        /// <summary>
        /// 根据任何地区ID，获取地区完整名称
        /// </summary>
        /// <param name="regionId">regionId</param>
        /// <param name="sep">拼接字符</param>
        /// <returns></returns>
        string GetFullNameById(string regionId, string sep = "");

        /// <summary>
        /// 根据regionId获取省份名称
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        string GetProvinceName(string regionId);

        /// <summary>
        /// 根据regionId获取城市名称
        /// </summary>
        /// <param name="regionId"></param>
        /// <returns></returns>
        string GetCityName(string regionId);
    }
}
