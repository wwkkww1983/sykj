/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：lh                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-02-20 11:49:47     
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.IServices                                   
*│　接口名称： IGuestbook                                   
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
	/// 留言信息表
	/// </summary>
    public interface IGuestbook: Sykj.Repository.IRepository<Sykj.Entity.Guestbook>
    {
       
    }
}