﻿using Sykj.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 缩略图
    /// </summary>
    public interface IThumbnailSize : Sykj.Repository.IRepository<Sykj.Entity.ThumbnailSize>
    {

        /// <summary>
        /// 从缓存中获取数据所有集合
        /// </summary>
        /// <returns>返回满足查询条件的list</returns>
        List<Sykj.Entity.ThumbnailSize> GetListByCache();

        /// <summary>
        /// 根据模块选择
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        List<Sykj.Entity.ThumbnailSize> GetThumSizeList(EnumHelper.UpLoadType type);
    }
}
