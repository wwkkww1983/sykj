﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Sykj.IServices;

namespace Sykj.Services
{
    /// <summary>
    /// 广告位置
    /// </summary>
    public class Advertiseposition : Sykj.Repository.RepositoryBase<Sykj.Entity.Advertiseposition>, Sykj.IServices.IAdvertiseposition
    {
        IAdvertisement _advertisement;

        public Advertiseposition(Sykj.Repository.SyDbContext dbcontext, Sykj.IServices.IAdvertisement advertisement) : base(dbcontext)
        {
            _advertisement = advertisement;
        }

        #region  删除广告位和其对应的广告内容
        /// <summary>
        /// 删除广告位和其对应的广告内容
        /// </summary>
        /// <param name="ids">广告位id的集合</param>
        /// <returns></returns>
        public bool Delete(List<int> ids)
        {
            //创建事物
            var tran = _dbContext.Database.BeginTransaction();
            try
            {
                //删除广告位
                var adpositionList = _dbContext.Advertiseposition.Where(c => ids.Contains(c.AdvPositionId));
                _dbContext.Advertiseposition.RemoveRange(adpositionList);
                _dbContext.SaveChanges();

                //根据广告位id删除广告内容
                var advertisementList = _dbContext.Advertisement.Where(c => ids.Contains(c.AdvPositionId));
                _dbContext.Advertisement.RemoveRange(advertisementList);
                _dbContext.SaveChanges();
                
                //提交
                tran.Commit();
                return true;
            }
            catch(Exception e)
            {
                //回滚
                tran.Rollback();
                return false;
            }
        }
        #endregion
    }
}
