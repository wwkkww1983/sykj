/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：推广表                                                    
*│　作    者：lh                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-04-03 10:51:43       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Services                                  
*│　类    名： Userinvite                                 
*└──────────────────────────────────────────────────────────────┘
*/
using Sykj.Infrastructure;
using Sykj.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Sykj.Services
{
    /// <summary>
    /// 推广表
    /// </summary>
    public class Userinvite : Sykj.Repository.RepositoryBase<Sykj.Entity.Userinvite>, Sykj.IServices.IUserinvite
    {
        public Userinvite(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }

        #region 分页查询
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public IQueryable<Sykj.Entity.Userinvite> GetPagedListExt(int pageIndex,
            int pageSize, string predicate, string ordering, params object[] args)
        {
            var result = from a in _dbContext.Userinvite
                         join b in _dbContext.Users on a.UserId equals b.UserId
                         join c in _dbContext.Users on a.InviteUserId equals c.UserId
                         select new
                         Sykj.Entity.Userinvite()
                         {
                             Id = a.Id,
                             UserId = a.UserId,
                             UserNick = a.UserNick,
                             InviteUserId = a.InviteUserId,
                             InviteNick = c.NickName,
                             Depth = a.Depth,
                             Path = a.Path,
                             IsRebate = a.IsRebate,
                             IsNew = a.IsNew,
                             RebateDesc = a.RebateDesc,
                             CreatedDate = a.CreatedDate,
                             Remark = a.Remark,
                             InviteType = a.InviteType,
                             InviteUserName = c.UserName,
                             UserName = b.UserName,
                         };
            if (!string.IsNullOrWhiteSpace(predicate))
                result = result.Where(predicate, args);
            if (!string.IsNullOrWhiteSpace(ordering))
                result = result.OrderBy(ordering);
            return result.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }
        #endregion

        #region 获得分页记录
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="predicate">条件</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public int GetCountExt(string predicate, params object[] args)
        {
            var result = from a in _dbContext.Userinvite
                         join b in _dbContext.Users on a.UserId equals b.UserId
                         join c in _dbContext.Users on a.InviteUserId equals c.UserId
                         select new
                         Sykj.Entity.Userinvite()
                         {
                             Id = a.Id,
                             UserId = a.UserId,
                             UserNick = a.UserNick,
                             InviteUserId = a.InviteUserId,
                             InviteNick =c.NickName,
                             Depth = a.Depth,
                             Path = a.Path,
                             IsRebate = a.IsRebate,
                             IsNew = a.IsNew,
                             RebateDesc = a.RebateDesc,
                             CreatedDate = a.CreatedDate,
                             Remark = a.Remark,
                             InviteType = a.InviteType,
                             InviteUserName = c.UserName,
                             UserName = b.UserName,
                         };
            if (!string.IsNullOrWhiteSpace(predicate))
                result = result.Where(predicate, args);
            return result.Count();
        }
        #endregion

        #region 获取分享邀请注册赢话费活动线上成功购买用户数量 ydp
        /// <summary>
        /// 获取分享邀请注册赢话费活动线上成功购买用户数量
        /// </summary>
        /// <param name="userId">邀请者id</param>
        /// <returns></returns>
        public int OnlineBuyNum(int userId)
        {
            var strSql = new StringBuilder();
            strSql.Append("SELECT ");
            strSql.Append(" Count(*) ");
            strSql.Append(" FROM accounts_userinvite a, shop_orders b  ");
            strSql.AppendFormat(" WHERE a.UserId=b.BuyerID AND a.InviteUserId={0} and a.InviteType=2 AND b.PaymentChannel IN('wxpay','alipay','wxjspay','alipaypc','wxnativepay') AND b.PaymentStatus=2  ", userId);
            var count = _dbContext.Database.SqlQuery(strSql.ToString());
            return count.ToString().ToInt();
        }
        #endregion

    }
}
